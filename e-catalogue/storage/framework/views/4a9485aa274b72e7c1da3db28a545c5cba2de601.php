<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/dashboard" class="brand-link">
      <img src="/img/icon ecatalogue.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Admin E-Catalogue</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/img/avatar5.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo e(auth()->user()->name); ?></a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/dashboard/" class="nav-link <?php echo e(($actived === "Dashboard" ? 'active' : '')); ?>">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/dashboard/categories" class="nav-link <?php echo e(($actived === "Kategori" ? 'active' : '')); ?>">
              <i class="nav-icon fas fa-barcode"></i>
              <p>
                Kategori
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/dashboard/companies" class="nav-link <?php echo e(($actived === "Perusahaan" ? 'active' : '')); ?>">
              <i class="nav-icon fas fa-handshake"></i>
              <p>
                Perusahaan
              </p>
            </a>
          </li>
          <li class="nav-header">Configurasi</li>
          <li class="nav-item">
            <a href="/dashboard/information" class="nav-link <?php echo e(($actived === "Pengumuman" ? 'active' : '')); ?>">
              <i class="nav-icon far fa-bell"></i>
              <p>
                Pengumuman
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/dashboard/account" class="nav-link <?php echo e(($actived === "Account" ? 'active' : '')); ?>">
              <i class="nav-icon far fa-user"></i>
              <p>
                Edit Account
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
<?php /**PATH C:\xampp\htdocs\e-catalogue\resources\views/dashboard/layouts/sidebar.blade.php ENDPATH**/ ?>