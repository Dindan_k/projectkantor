<?php $__env->startSection('container'); ?>
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Update Data</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
                <li class="breadcrumb-item active">Update Data Products</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <h3 class="card-title">
                        Company
                    </h3>
                </div>

              <!-- /.card-header -->
                <div class="card-body">
                <form action="/dashboard/companies/products/<?php echo e($item->slug); ?>/edit" method="post" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                    <div class="form-group">
                        <label for="name" class="form-label">Nama Product</label>
                        <input type="text" class="form-control <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="name" name="name" required value="<?php echo e(old('name', $item->name)); ?>" autofocus>
                        <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?><label class="col-form-label" for="name"><i class="fas fa-circle"></i><?php echo e($message); ?></label><?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>

                    <div class="">
                        <input type="" name="slug" class="form-control" id="slug" required value="<?php echo e(old('slug', $item->slug)); ?>">
                    </div>

                    <div class="form-group">
                    <label for="company_id" class="form-label">Pilih Perusahaan</label>
                        <select class="form-select form-control select2" name="company_id">
                          <?php if(old('company_id', $item->id) == $item->id): ?>
                           <option value="<?php echo e($item->id); ?>" selected><?php echo e($item->company->name); ?></option>
                          <?php else: ?>
                           <option value="<?php echo e($item->id); ?>"><?php echo e($item->company->name); ?></option>
                           <?php endif; ?>

                        </select>
                        <?php $__errorArgs = ['company_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?><label class="col-form-label" for="company_id"><i class="fas fa-circle"></i><?php echo e($message); ?></label><?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>


                    <div class="form-group" class="form-label">
                        <label for="price" class="form-label">Price</label>
                        <input type="text" class="form-control  <?php $__errorArgs = ['price'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="price" name="price" required value="<?php echo e(old('price', $item->price)); ?>" autofocus>
                        <?php $__errorArgs = ['price'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?><label class="col-form-label" for="price"><i class="fas fa-circle"></i><?php echo e($message); ?></label><?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                      </div>


                      <div class="form-group">
                        <label for="image" class="form-label">Gambar</label>
                        <input type="hidden" name="oldImage" value="<?php echo e($item->image); ?>">
                        <?php if($item->image): ?>
                        <img src="<?php echo e(asset('storage/' . $item->image)); ?>" class="img-preview img-fluid mb-3 col-sm-5 d-block">
                        <?php else: ?>
                        <img class="img-preview img-fluid mb-3 col-sm-5">
                        <?php endif; ?>
                            <div class="input-group">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" type="file" id="image" name="image" onchange="previewImage()">
                                <label class="custom-file-label" for="image">Choose file</label>
                              </div>
                            </div>
                            <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?><label class="col-form-label" for="image"><i class="fas fa-circle"></i><?php echo e($message); ?></label><?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>

                    <div class="form-group">
                    <label for="desc" class="form-label">Keterangan</label>
                    <?php $__errorArgs = ['desc'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> <p class="text-danger"><?php echo e($message); ?></p> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    <input type="hidden" class="form-control  <?php $__errorArgs = ['desc'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="desc" name="desc" value="">
                    <textarea id="summernote" name="desc"><?php echo e(old('desc', $item->desc)); ?></textarea>
                    </div>

                    <button type="submit" class="btn btn-primary swalDefaultSuccess"><i class="fas fa-save"></i> Simpan Data</button>
                    <button hreff="/dashboard/companies" type="button" class="btn btn-secondary">Batal</button>
                </form>
                </div>

              <!-- /.card-header -->
            </div>
        </div>
    </div>
</section>
<script>
     $('#name').change(function(e) {
       $.get('<?php echo e(url('/dashboard/companies/products/check_slug')); ?>',
       { 'name': $(this).val() },
       function( data ) {
           $('#slug').val(data.slug);
       }
       );
    });

     function previewImage() {
    const image = document.querySelector('#image');
    const imgPreview = document.querySelector('.img-preview');

    imgPreview.style.display = 'block';

    const oFReader = new FileReader();
    oFReader.readAsDataURL(image.files[0]);

    oFReader.onload = function(oFREvent) {
      imgPreview.src = oFREvent.target.result;
    }

  }
</script>
<!-- /.content -->
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('dashboard.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\e-catalogue\resources\views/dashboard/products/edit.blade.php ENDPATH**/ ?>