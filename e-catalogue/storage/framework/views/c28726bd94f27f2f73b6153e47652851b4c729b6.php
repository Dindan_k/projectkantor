<?php $__env->startSection('container'); ?>
<section id="company">
    <div class="container">
        <div class="row mb-4">
            <div class="col-12 text-center">
                <h2 class="kategori-font">Perusahaan</h2>
                <span class="sub-title">Silahkan pilih Perusahaan</span>
            </div>
        </div>
        <div class="container">

            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
              <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div class="col">
                <div class="card shadow-sm" style="height: 500px">
                  <img class="bd-placeholder-img card-img-top" width="100%" height="225" src="/storage/<?php echo e($company->image); ?>" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false">

                  <div class="card-body">
                  <h4><?php echo e($company->name); ?></h4>
                    <p class="card-text"><?php echo Str::limit($company->desc, 250, ' ....'); ?></p>
                    <br>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group mx-auto">
                        <a href="/companies/details" type="button" class="btn btn-sm btn-outline-secondary">View</a>
                        <a type="button" class="btn btn-sm btn-outline-secondary">Edit</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
          </div>
    </div>
</section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\e-catalogue\resources\views/f-company.blade.php ENDPATH**/ ?>