<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
            <div class="owl-carousel owl-theme">
                <div class="item"><img src="/img/avatar.png" alt=""></div>
                <div class="item"><img src="/img/avatar.png" alt=""></div>
                <div class="item"><img src="/img/avatar.png" alt=""></div>
                <div class="item"><img src="/img/avatar.png" alt=""></div>
                <div class="item"><img src="/img/avatar.png" alt=""></div>
                <div class="item"><img src="/img/avatar.png" alt=""></div>
                <div class="item"><img src="/img/avatar.png" alt=""></div>
                <div class="item"><img src="/img/avatar.png" alt=""></div>
                <div class="item"><img src="/img/avatar.png" alt=""></div>
                <div class="item"><img src="/img/avatar.png" alt=""></div>
                <div class="item"><img src="/img/avatar.png" alt=""></div>
                <div class="item"><img src="/img/avatar.png" alt=""></div>
            </div>
    </div>
    <script>
        $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
            }
        })
    </script>
    <script src="/js/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="/js/owl.carousel.js"></script>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\e-catalogue\resources\views/percobaan.blade.php ENDPATH**/ ?>