@extends('layouts.main')
@section('container')
<!-- Hero -->
<section id="hero">
    <div class="container h-100">
        <div class="row h-100">
            <div class="col-md-6 hero-tagline my-auto">
                <h1>Selamat datang di E-catalogue</h1>
                <p>Ini adalah aplikasi E-catalogue dari UKPBJ BKN</p>
                <button class="button-lg">Lihat</button>
            </div>
        </div>
        <img src="/img/2.png" alt="" class="position-absolute end-0 bottom-0 img-hero" width="50%">
        <img src="/img/1.png" alt="" class="h-100 position-absolute top-0 start-0 asset">
    </div>
</section>
<!-- End Hero -->

<!-- Kategori -->
<section id="kategori">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center pb-2">
                <h2 class="kategori-font">Kategori</h2>
                <span class="sub-title">Silahkan pilih kategori</span>
            </div>
        </div>
        {{-- Slider --}}
        <!-- subjects section starts  -->
        <section class="subjects">
            <div class="box-container">
                <div class="owl-carousel">
                    @foreach ($categories as $category)
                    <div class="box">
                        <a href="">{{ $category->name }}</a>
                        <p>{{ $category->companies->count() }} Company</p>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- subjects section ends -->
        {{-- End Slider --}}
    </div>
</section>
<!-- End Kategori -->

<!-- search -->
<section id="search">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="">Pencarian Data</h2>
            </div>
        </div>
    </div>
    <div class="col-10 mx-auto pb-4">
        <label for="exampleDataList" class="form-label"><h3>Datalist example</h3></label>
        <input class="form-control" list="datalistOptions" id="exampleDataList" placeholder="Type to search...">
        <datalist id="datalistOptions">
        <option value="San Francisco">
        <option value="New York">
        <option value="Seattle">
        <option value="Los Angeles">
        <option value="Chicago">
        </datalist>
    </div>
    <div class="col-10 mx-auto">
        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <button type="button" class="btn-type mb-2">Button 1</button>
                <button type="button" class="btn-type mb-2">Button 1</button>
                <button type="button" class="btn-type mb-2">Button 1</button>
                <button type="button" class="btn-type mb-2">Button 1</button>
                <button type="button" class="btn-type mb-2">Button 1</button>
                <button type="button" class="btn-type mb-2">Button 1</button>
                <button type="button" class="btn-type mb-2">Button 1</button>
                <button type="button" class="btn-type mb-2">Button 1</button>
              </div>
              <div class="carousel-item">
                <button type="button" class="btn-type mb-2">Button 2</button>
                <button type="button" class="btn-type mb-2">Button 2</button>
                <button type="button" class="btn-type mb-2">Button 2</button>
                <button type="button" class="btn-type mb-2">Button 2</button>
                <button type="button" class="btn-type mb-2">Button 2</button>
                <button type="button" class="btn-type mb-2">Button 2</button>
                <button type="button" class="btn-type mb-2">Button 2</button>
                <button type="button" class="btn-type mb-2">Button 2</button>
              </div>
              <div class="carousel-item">
                <button type="button" class="btn-type mb-2">Button 3</button>
                <button type="button" class="btn-type mb-2">Button 3</button>
                <button type="button" class="btn-type mb-2">Button 3</button>
                <button type="button" class="btn-type mb-2">Button 3</button>
                <button type="button" class="btn-type mb-2">Button 3</button>
                <button type="button" class="btn-type mb-2">Button 3</button>
                <button type="button" class="btn-type mb-2">Button 3</button>
                <button type="button" class="btn-type mb-2">Button 3</button>
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
</section>
<!-- End search -->

<!-- New Product -->
<section id="newproduct">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2> New Product</h2>
            </div>
        </div>
            <div class="container">
                <div class="row">
                    <div class="owl-carousel">
                        <div class="item">
                            <div class="card">
                                <img src="/img/avatar.png" class="card-img-top">
                                <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                </div>
                                <div class="card-footer">
                                <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <img src="/img/avatar2.png" class="card-img-top">
                                <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                                </div>
                                <div class="card-footer">
                                <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card">
                                <img src="/img/avatar3.png" class="card-img-top">
                                <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                </div>
                                <div class="card-footer">
                                <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>
<!-- End New Product -->
@endsection
