@extends('dashboard.layouts.main')
@section('container')
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Update Data Category</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
                <li class="breadcrumb-item"><a href="/dashbard/categories">Category</a></li>
                <li class="breadcrumb-item active">Update Data Category</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <h3 class="card-title">
                        Category
                    </h3>
                </div>

              <!-- /.card-header -->
                <div class="card-body">
                    <form action="/dashboard/categories/{{ $category->slug }}" method="post" enctype="multipart/form-data">
                        @method('put')
                        @csrf
                    <div class="form-group">
                        <label for="name" class="form-label">Nama Kategori</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" required value="{{ old('name', $category->name) }}" autofocus>
                        @error('name')<label class="col-form-label" for="name"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="">
                        <input type="hidden" name="slug" class="form-control" id="slug" required value="{{ old('slug', $category->slug) }}>
                    </div>

                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Update Data</button>
                </form>
                </div>

              <!-- /.card-header -->
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $('#name').change(function(e) {
       $.get('{{ url('/dashboard/categories/check_slug') }}',
       { 'name': $(this).val() },
       function( data ) {
           $('#slug').val(data.slug);
       }
       );
    });

    function previewImage() {
    const image = document.querySelector('#image');
    const imgPreview = document.querySelector('.img-preview');

    imgPreview.style.display = 'block';

    const oFReader = new FileReader();
    oFReader.readAsDataURL(image.files[0]);

    oFReader.onload = function(oFREvent) {
      imgPreview.src = oFREvent.target.result;
    }

  }
</script>
<!-- /.content -->
</div>
@endsection
