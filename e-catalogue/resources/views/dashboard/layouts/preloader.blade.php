<div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="/img/icon ecatalogue.png" alt="AdminLTELogo" height="60" width="60">
    <h1 class="animation__shake">Selamat Datang {{ auth()->user()->name }}</h1>
</div>
