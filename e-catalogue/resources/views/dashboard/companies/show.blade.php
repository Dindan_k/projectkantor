@extends('dashboard.layouts.main')
@section('container')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Company Detail</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
              <li class="breadcrumb-item"><a href="/dashboard/companies">DataTables Company</a></li>
              <li class="breadcrumb-item active">Company Detail</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Company Detail</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
              <div class="row">
                <div class="col-12">
                    <div class="info-box">
                        <div class="info-box-content">
                        <img src="/storage/{{ $company->image }}" class="product-image" alt="Product Image">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-4">
                  <div class="info-box bg-light">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-muted">Barang</span>
                      <span class="info-box-number text-center text-muted mb-0">{{ $items->count() }}</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-4">
                  <div class="info-box bg-light">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-muted">Pengalaman Kerja</span>
                      <span class="info-box-number text-center text-muted mb-0">0</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-4">
                  <div class="info-box bg-light">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-muted">Estimated project duration</span>
                      <span class="info-box-number text-center text-muted mb-0">20</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
              <h3 class="text-primary"><i class="fas fa-globe"></i> {{ $company->name }}</h3>
              <p class="text-muted">{!! $company->desc !!}</p>
              <br>
              {{-- <div class="text-muted">
                <p class="text-sm">Client Company
                  <b class="d-block">-</b>
                </p>
                <p class="text-sm">Project Leader
                  <b class="d-block">-</b>
                </p>
              </div> --}}

              <h5 class="mt-5 text-muted">Profile Company</h5>
              <ul class="list-unstyled">
                <li>
                  <p><i class="fas fa-home"></i> : {{ $company->address }}</p>
                </li>
                <li>
                  <p><i class="fas fa-envelope"></i> : {{ $company->email }}</p>
                </li>
                <li>
                  <p><i class="fas fa-phone"></i> : {{ $company->tlpn }}</p>
                </li>
              </ul>
              <div class="text-center mt-5 mb-3">
                <a href="/dashboard/companies/products/{{ $company->slug }}" class="btn btn-sm btn-primary">Add files</a>
                <a href="/dashboard/companies/products/{{ $company->slug }}/print" class="btn btn-sm btn-warning"> <i class="fas fa-print"></i> Print</a>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Product Company</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            @foreach ($items as $item)
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
              <div class="card bg-light d-flex flex-fill">
                <div class="card-header text-muted border-bottom-0">
                  {{ $company->category->name }}
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-7">
                      <h2 class="lead"><b>Nama Product: </b> {{ $item->name }}</h2>
                      <p class="text-muted text-sm">Desc: {!! $item->desc !!} </p>
                      <ul class="ml-4 mb-0 fa-ul text-muted">
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Address: Demo Street 123, Demo City 04312, NJ</li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-money-bill"></i></span> {{ $item->price }}</li>
                      </ul>
                    </div>
                    <div class="col-5 text-center">
                      <img src="/storage/{{ $item->image }}" alt="user-avatar" class="img-circle img-fluid">
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="/dashboard/companies/products/{{ $item->slug }}/edit" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> Edit
                    </a>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <nav aria-label="Contacts Page Navigation">
            <ul class="pagination justify-content-center m-0">
              <li class="page-item active"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item"><a class="page-link" href="#">4</a></li>
              <li class="page-item"><a class="page-link" href="#">5</a></li>
              <li class="page-item"><a class="page-link" href="#">6</a></li>
              <li class="page-item"><a class="page-link" href="#">7</a></li>
              <li class="page-item"><a class="page-link" href="#">8</a></li>
            </ul>
          </nav>
        </div>
        <!-- /.card-footer -->
      </div>

      <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
@endsection
