@extends('dashboard.layouts.main')
@section('container')
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Create Data</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
                <li class="breadcrumb-item"><a href="?dashboard/companies">DataTables Company</a></li>
                <li class="breadcrumb-item active">Create Data Company</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <h3 class="card-title">
                        Company
                    </h3>
                </div>

              <!-- /.card-header -->
                <div class="card-body">
                <form action="/dashboard/companies" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="form-group">
                        <label for="name" class="form-label">Nama Perusahaan</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" required value="{{ old('name') }}" autofocus>
                        @error('name')<label class="col-form-label" for="name"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="form-group">
                    <label for="category_id" class="form-label">Pilih Kategori</label>
                        <select class="form-select form-control select2" name="category_id">
                          @foreach ($categories as $category)
                          @if (old('category_id') == $category->id)
                           <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                          @else
                           <option value="{{ $category->id }}">{{ $category->name }}</option>
                           @endif
                          @endforeach
                        </select>
                        @error('category_id')<label class="col-form-label" for="name"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="form-group">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" required value="{{ old('email') }}" autofocus>
                        @error('email')<label class="col-form-label" for="email"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="form-group" class="form-label">
                        <label for="tlpn" class="form-label">Nomor Telpon</label>
                        <input type="text" class="form-control  @error('tlpn') is-invalid @enderror" id="tlpn" name="tlpn" required value="{{ old('tlpn') }}" autofocus>
                        @error('tlpn')<label class="col-form-label" for="tlpn"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                      </div>

                    <div class="form-group">
                        <label for="address" class="form-label">Alamat Perusahaan</label>
                        <input type="textarea" name="address" class="form-control @error('address') is-invalid @enderror" id="address" name="address" required value="{{ old('address') }}" autofocus>
                        @error('address')<label class="col-form-label" for="address"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="form-group">
                    <label for="image" class="form-label">Gambar</label>
                    <img class="img-preview img-fluid mb-3 col-sm-5">
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input @error('image') is-invalid @enderror" type="file" id="image" name="image" onchange="previewImage()">
                            <label class="custom-file-label" for="image">Choose file</label>
                          </div>
                        </div>
                        @error('image')<label class="col-form-label" for="image"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="form-group">
                    <label for="desc" class="form-label">Keterangan</label>
                    @error('desc') <p class="text-danger">{{ $message }}</p> @enderror
                    <input type="hidden" class="form-control  @error('desc') is-invalid @enderror" id="desc" name="desc" value="">
                    <textarea id="summernote" name="desc">{{ old('desc') }}</textarea>
                    </div>

                    <button type="submit" class="btn btn-primary swalDefaultSuccess"><i class="fas fa-save"></i> Simpan Data</button>
                    <button hreff="/dashboard/companies" type="button" class="btn btn-secondary">Batal</button>
                </form>
                </div>

              <!-- /.card-header -->
            </div>
        </div>
    </div>
</section>
<script>
     function previewImage() {
    const image = document.querySelector('#image');
    const imgPreview = document.querySelector('.img-preview');

    imgPreview.style.display = 'block';

    const oFReader = new FileReader();
    oFReader.readAsDataURL(image.files[0]);

    oFReader.onload = function(oFREvent) {
      imgPreview.src = oFREvent.target.result;
    }

  }
</script>
<!-- /.content -->
</div>
@endsection
