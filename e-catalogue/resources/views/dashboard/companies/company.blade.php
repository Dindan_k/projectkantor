@extends('dashboard.layouts.main')
@section('container')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!-- Alert Sukses-->
        @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
           <h5><i class="icon fas fa-check"></i> Alert!</h5>
           {{ session('success') }}
        </div>
        @endif
        <!-- End Alert Sukses-->
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <a class="btn btn-app" href="/dashboard/companies/create">
                <i class="fas fa-plus"></i> Tambah Data
            </a>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
              <li class="breadcrumb-item active">DataTables Company</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">DataTable Company</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>NO</th>
                    <th>Nama Perusahaan</th>
                    <th>Kategori</th>
                    <th>Keterangan</th>
                    <th>Alamat</th>
                    <th>Email</th>
                    <th>No Telpon</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach ($companies as $company)
                  <tr>
                    <th>{{ $loop->iteration }}</th>
                    <td>{{ $company->name }}</td>
                    <td>{{ $company->category->name }}</td>
                    <td>{!! $company->desc !!}</td>
                    <td>{{ $company->address }}</td>
                    <td>{{ $company->email }}</td>
                    <td>{{ $company->tlpn }}</td>
                    <td>
                        <a href="/dashboard/companies/{{ $company->slug }}" class="badge bg-info"><i class="fas fa-eye"></i></a>
                        <a href="/dashboard/companies/{{ $company->slug }}/edit" class="badge bg-info"><i class="fas fa-edit"></i></a>
                        <form action="/dashboard/companies/{{ $company->slug }}" method="post" class="d-inline">
                            @method('delete')
                            @csrf
                            <button class="badge bg-danger border-0" onclick="return confirm('Apa kamu yakin mau menghapus ini?')
                            "><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
