@extends('dashboard.layouts.main')
@section('container')
@extends('dashboard.layouts.main')
@section('container')
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Update Data</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
                <li class="breadcrumb-item active">Update Data Products</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <h3 class="card-title">
                        Company
                    </h3>
                </div>

              <!-- /.card-header -->
                <div class="card-body">
                <form action="/dashboard/companies/products/{{ $users->slug }}/edit" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="form-group">
                        <label for="name" class="form-label">Nama Lengkap</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" required value="{{ old('name', $users->name) }}" autofocus>
                        @error('name')<label class="col-form-label" for="name"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="form-group">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" name="username" required value="{{ old('username', $users->username) }}" autofocus>
                        @error('username')<label class="col-form-label" for="username"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="form-group" class="form-label">
                        <label for="email" class="form-label">Email</label>
                        <input type="text" class="form-control  @error('email') is-invalid @enderror" id="email" name="email" required value="{{ old('email', $users->email) }}" autofocus>
                        @error('email')<label class="col-form-label" for="email"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="form-group">
                      <label for="image" class="form-label">Gambar</label>
                      <input type="hidden" name="oldImage" value="{{ $users->image }}">
                      @if ($users->image)
                      <img src="{{ asset('storage/' . $users->image) }}" class="img-preview img-fluid mb-3 col-sm-5 d-block">
                      @else
                      <img class="img-preview img-fluid mb-3 col-sm-5">
                      @endif
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input @error('image') is-invalid @enderror" type="file" id="image" name="image" onchange="previewImage()">
                          <label class="custom-file-label" for="image">Choose file</label>
                        </div>
                      </div>
                        @error('image')<label class="col-form-label" for="image"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="form-group" class="form-label">
                        <label for="password" class="form-label">Password</label>
                        <input type="text" class="form-control  @error('password') is-invalid @enderror" id="password" name="password" required value="{{ old('password', $users->password) }}" autofocus>
                        @error('password')<label class="col-form-label" for="password"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <button type="submit" class="btn btn-primary swalDefaultSuccess"><i class="fas fa-save"></i> Simpan Data</button>
                    <button hreff="/dashboard/companies" type="button" class="btn btn-secondary">Batal</button>
                </form>
                </div>

              <!-- /.card-header -->
            </div>
        </div>
    </div>
</section>
<script>
     $('#name').change(function(e) {
       $.get('{{ url('/dashboard/companies/products/check_slug') }}',
       { 'name': $(this).val() },
       function( data ) {
           $('#slug').val(data.slug);
       }
       );
    });

     function previewImage() {
    const image = document.querySelector('#image');
    const imgPreview = document.querySelector('.img-preview');

    imgPreview.style.display = 'block';

    const oFReader = new FileReader();
    oFReader.readAsDataURL(image.files[0]);

    oFReader.onload = function(oFREvent) {
      imgPreview.src = oFREvent.target.result;
    }

  }
</script>
<!-- /.content -->
</div>
@endsection

@endsection
