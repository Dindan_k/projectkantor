@extends('dashboard.layouts.main')
@section('container')
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Update Data</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard/">Home</a></li>
                <li class="breadcrumb-item active">Update Information</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <h3 class="card-title">
                        Company
                    </h3>
                </div>

              <!-- /.card-header -->
                <div class="card-body">
                <form action="/dashboard/information" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="form-group">
                        <label for="title" class="form-label">Judul</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" required value="{{ old('title') }}" autofocus>
                        @error('title')<label class="col-form-label" for="title"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="form-group">
                    <label for="company_id" class="form-label">Pilih Perusahaan</label>
                        <select class="form-select form-control select2" name="company_id">
                          @foreach ($companies as $company)
                          @if (old('company_id') == $company->id)
                           <option value="{{ $company->id }}" selected>{{ $company->name }}</option>
                          @else
                           <option value="{{ $company->id }}">{{ $company->name }}</option>
                           @endif
                          @endforeach
                        </select>
                        @error('company_id')<label class="col-form-label" for="company_id"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="form-group">
                    <label for="image" class="form-label">Gambar</label>
                    <img class="img-preview img-fluid mb-3 col-sm-5">
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input @error('image') is-invalid @enderror" type="file" id="image" name="image" onchange="previewImage()">
                            <label class="custom-file-label" for="image">Choose file</label>
                          </div>
                        </div>
                        @error('image')<label class="col-form-label" for="image"><i class="fas fa-circle"></i>{{ $message }}</label>@enderror
                    </div>

                    <div class="form-group">
                    <label for="desc" class="form-label">Keterangan</label>
                    @error('desc') <p class="text-danger">{{ $message }}</p> @enderror
                    <input type="hidden" class="form-control  @error('desc') is-invalid @enderror" id="desc" name="desc" value="">
                    <textarea id="summernote" name="desc">{{ old('desc') }}</textarea>
                    </div>

                    <button type="submit" class="btn btn-primary swalDefaultSuccess"><i class="fas fa-save"></i> Simpan Data</button>
                    <button hreff="/dashboard/companies" type="button" class="btn btn-secondary">Batal</button>
                </form>
                </div>

              <!-- /.card-header -->
            </div>
        </div>
    </div>
</section>
<script>
    function previewImage() {
    const image = document.querySelector('#image');
    const imgPreview = document.querySelector('.img-preview');

    imgPreview.style.display = 'block';

    const oFReader = new FileReader();
    oFReader.readAsDataURL(image.files[0]);

    oFReader.onload = function(oFREvent) {
      imgPreview.src = oFREvent.target.result;
    }

  }
</script>
<!-- /.content -->
</div>
@endsection
