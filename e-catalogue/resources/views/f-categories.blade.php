@extends('layouts.main')
@section('container')
<section id="company">
    <div class="container">
        <div class="row mb-4">
            <div class="col-12 text-center">
                <h2 class="kategori-font">Kategories: </h2>
                <span class="sub-title">Silahkan pilih Perusahaan</span>
            </div>
        </div>
        <div class="row">
            @foreach ($companies as $company)
            <div class="col-4 mb-4">
                <div class="card p-2" style="width: 22rem">
                    <img src="/storage/{{ $company->image }}" alt="">
                    <div class="card-body">
                        <h4>{{ $company->name }}</h4>
                        <p>{!! Str::limit($company->desc, 250, ' ....') !!}</p>
                    </div>
                    <div class="card-button">
                        <button> View </button>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
