    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-transparent position-fixed w-100">
        <div class="container">
          <a class="navbar-brand" href="#">
            <img src="/img/icon ecatalogue.png" alt="" width="30" class="d-inline-block align-text-top">
             <b>E-Catalogue</b> UKPBJ
            </a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mx-auto">
              <li class="nav-item">
                <a class="nav-link {{ ($actived === "Home" ? 'active' : '') }}" href="/">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{ ($actived === "Company" ? 'active' : '') }}" href="/companies">Company</a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{ ($actived === "Contact" ? 'active' : '') }}" href="/contact">Contact</a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{ ($actived === "About Me" ? 'active' : '') }}" href="/aboutme">About Me</a>
              </li>
            </ul>
          </div>
        </div>
    </nav>
    <!-- End Navbar -->
