@extends('layouts.main')
@section('container')
<section id="company">
    <div class="container">
        <div class="row mb-4">
            <div class="col-12 text-center">
                <h2 class="kategori-font">Perusahaan</h2>
                <span class="sub-title">Silahkan pilih Perusahaan</span>
            </div>
        </div>
        <div class="container">

            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
              @foreach ($companies as $company)
              <div class="col">
                <div class="card shadow-sm" style="height: 500px">
                  <img class="bd-placeholder-img card-img-top" width="100%" height="225" src="/storage/{{ $company->image }}" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false">

                  <div class="card-body">
                  <h4>{{ $company->name }}</h4>
                    <p class="card-text">{!! Str::limit($company->desc, 250, ' ....') !!}</p>
                    <br>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group mx-auto">
                        <a href="/companies/details" type="button" class="btn btn-sm btn-outline-secondary">View</a>
                        <a type="button" class="btn btn-sm btn-outline-secondary">Edit</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
    </div>
</section>

@endsection
