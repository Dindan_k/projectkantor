<?php

use App\Models\Company;
use App\Models\Category;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DascompController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\DashcateController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\InformationController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\PrintController;
use App\Http\Controllers\AccountController;
use App\Models\Item;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index']);
Route::get('/companies', [FrontendController::class, 'companies']);
Route::get('/contact', [FrontendController::class, 'contact']);
Route::get('/aboutme', [FrontendController::class, 'aboutme']);
Route::get('/companies/details', [FrontendController::class, 'details']);

Route::resource('/registration', RegisterController::class)->middleware('guest');
Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'login']);
Route::post('/logout', [LoginController::class, 'logout']);

Route::get('/dashboard', function(){ return view('dashboard.dashboard', [
    'categories' => Category::all(),
    'companies' => Company::all(),
    'products' => Item::all(),
    'actived' => 'Dashboard'
]);})->middleware('auth');

Route::get('/dashboard/companies/check_slug', [DascompController::class, 'check_slug'])->middleware('auth');
Route::resource('/dashboard/companies', DascompController::class)->middleware('auth');

Route::get('/dashboard/categories/check_slug', [DashcateController::class, 'check_slug'])->middleware('auth');
Route::resource('/dashboard/categories', DashcateController::class)->middleware('auth');


Route::get('/dashboard/companies/products/check_slug', [ItemController::class, 'check_slug'])->middleware('auth');
Route::get('/dashboard/companies/products/{company:slug}', [ItemController::class, 'create'])->middleware('auth');
Route::get('/dashboard/companies/products/{company:slug}/print', [ItemController::class, 'show'])->middleware('auth');
Route::get('/dashboard/companies/products/{item:slug}/edit', [ItemController::class, 'edit'])->middleware('auth');
Route::post('/dashboard/companies/products/{item:slug}/edit', [ItemController::class, 'update'])->middleware('auth');
Route::resource('/dashboard/companies/products', ItemController::class)->middleware('auth');

Route::resource('/dashboard/information', InformationController::class)->middleware('auth');
Route::resource('/dashboard/account', AccountController::class)->middleware('auth');
