<?php

namespace App\Http\Controllers;

use App\Models\Information;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class InformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.informations.create', [
            'companies' => Company::all(),
            'actived' => 'Pengumuman'

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validasi = $request->validate
        ([
            'title' => 'required|max:225',
            'company_id' => 'required',
            'desc' => 'required|min:4',
            'image' => 'image|file|max:1024'
        ]);
        if($request->file('image')){
            $validasi['image'] = $request->file('image')->store('images');
        }
        Information::create($validasi);
        return redirect('/dashboard/companies')->with('success', 'Anda telah berhasil menambah data perusahaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function show(Information $information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function edit(Information $information)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Information $information)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function destroy(Information $information)
    {
        //
    }
}
