<?php

namespace App\Http\Controllers;
use App\Models\Company;
use App\Models\Category;

use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index(Company $company)
    {
        return view('index',[
            'categories' => Category::all(),
            'companies' => $company->companies,
            'actived' => 'Home'

        ]);
    }
    public function companies(Company $company)
    {
        return view('f-company',[
            'companies' => $company::all(),
            'actived' => 'Company'

        ]);
    }
    public function contact(Request $request)
    {
        return view('f-contact', [
            'actived' => 'Contact'
        ]);
    }
    public function aboutme(Request $request)
    {
        return view('f-aboutme',[
            'actived' => 'About Me'
        ]);
    }
    public function details(Request $request)
    {
        return view('details-company',[
            'actived' => 'Company'
        ] );
    }
}
