<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Cviebrock\EloquentSluggable\Services\SlugService;

class DascompController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.companies.company', [
            'companies' => Company::latest()->filter(request(['search']))->get(),
            'actived' => 'Perusahaan'

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.companies.create', [
            'categories' => Category::all(),
            'actived' => 'Perusahaan'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validasi = $request->validate
        ([
            'name' => 'required|max:225',
            'category_id' => 'required',
            'desc' => 'required|min:4',
            'tlpn' => 'required',
            'email' => 'required|email:dns|unique:companies',
            'address' => 'required|min:10',
            'image' => 'image|file|max:1024'
        ]);
        if($request->file('image')){
            $validasi['image'] = $request->file('image')->store('images');
        }
        Company::create($validasi);
        return redirect('/dashboard/companies')->with('success', 'Anda telah berhasil menambah data perusahaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('dashboard.companies.show', [
            //untuk menampilkan data yang beralasi di model company
            'items' => $company->items,
            //
            'company' => $company,
            'actived' => 'Perusahaan'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('dashboard.companies.edit',[
            'categories' => Category::all(),
            'company' => $company,
            'actived' => 'Perusahaan'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $rules = [
            'name' => 'required|max:225',
            'category_id' => 'required',
            'desc' => 'required|min:4',
            'address' => 'required|min:10',
            'image' => 'image|file|max:1024'
        ];

        if($request->slug != $company->slug){
            $rules['slug'] = 'required|unique:companies';
        }

        if($request->email != $company->email){
            $rules['email'] = 'required|unique:companies';
        }

        if($request->tlpn != $company->tlpn){
            $rules['tlpn'] = 'required|unique:companies';
        }

        $validasi = $request->validate($rules);
        if($request->file('image')){
            if($request->oldImage){
                Storage::delete($request->oldImage);
            }
            $validasi['image'] = $request->file('image')->store('images');
        }

        Company::where('id', $company->id)
        ->update($validasi);
        return redirect('/dashboard/companies')->with('success', 'Anda telah berhasil merubah data perusahaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        if($company->image) {
            Storage::delete($company->oldImage);
        }
        Company::destroy($company->id);
        return redirect('/dashboard/companies')->with('success', 'Anda telah berhasil hapus data perusahaan');
    }

    public function check_slug(Request $request)
    {
        $slug = SlugService::createSlug(Company::class, 'slug', $request->name);
        return response()->json(['slug' => $slug]);
    }
}
