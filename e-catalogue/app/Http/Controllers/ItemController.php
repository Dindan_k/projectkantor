<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Cviebrock\EloquentSluggable\Services\SlugService;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Item $item, Company $company)
    {
        return view('dashboard.products.create',[
            'companies' => Company::all(),
            'company' => $company,
            'actived' => 'Perusahaan'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validasi = $request->validate
        ([
            'name' => 'required|max:225',
            'company_id' => 'required',
            'price' => 'required',
            'desc' => 'required|min:4',
            'image' => 'image|file|max:1024'
        ]);
        if($request->file('image')){
            $validasi['image'] = $request->file('image')->store('images');
        }
        Item::create($validasi);
        return redirect('/dashboard/companies')->with('success', 'Anda telah berhasil menambah data perusahaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item, Company $company)
    {
        return view('dashboard.products.print', [
            'items' => $company->items,
            //
            'company' => $company,
            'actived' => 'Perusahaan'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item, Company $company)
    {
        return view('dashboard.products.edit',[
            'companies' => Company::all(),
            'company' => $company,
            'item' => $item,
            'actived' => 'Perusahaan'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $rules = [
            'name' => 'required|max:225',
            'desc' => 'required|min:4',
            'price' => 'required|min:6',
            'image' => 'image|file|max:1024'
        ];

        if($request->slug != $item->slug){
            $rules['slug'] = 'required|unique:items';
        }

        $validasi = $request->validate($rules);
        if($request->file('image')){
            if($request->oldImage){
                Storage::delete($request->oldImage);
            }
            $validasi['image'] = $request->file('image')->store('images');
        }

        Item::where('id', $item->id)
        ->update($validasi);
        return redirect('/dashboard/companies')->with('success', 'Anda telah berhasil merubah data perusahaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        //
    }

    public function check_slug(Request $request)
    {
        $slug = SlugService::createSlug(Item::class, 'slug', $request->name);
        return response()->json(['slug' => $slug]);
    }
    
    public function printpdf(Request $request)
    {
        //
    }
}
