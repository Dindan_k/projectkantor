<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('dashboard.index');
    }
    public function login(Request $request)
    {
       $credentials = $request->validate
        ([
            'email' => 'required',
            'password' => 'required'
        ]);

        if (Auth::attempt($credentials))
        {
            $request->session()->regenerate();

            return redirect()->intended('/dashboard')->with('success', 'Gagal Login Pastikan lagi Email dan Passwordnya');
        }
        return back()->with('LoginError', 'Gagal Login Pastikan lagi Email dan Passwordnya');
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
