<?php

namespace Database\Seeders;
use App\Models\Category;
use App\Models\Company;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Category::create([
            'name' => 'Jasa',
            'slug' => 'jasa'
        ]);
        Category::create([
            'name' => 'Barang',
            'slug' => 'barang'
        ]);
        Category::create([
            'name' => 'Kebutuhan Mendesak',
            'slug' => 'kebutuhan-mendesak'
        ]);

         //ini isian perusahaan
         Company::create([
            'name' => 'Ivana',
            'category_id' => '1',
            'desc' => 'Notaris',
            'tlpn' => '8228978',
            'email'=> 'ivana@gmail.com',
            'slug' => 'ivana',
            'address' => 'Jalan perintisan bajdk',
            'image' => ''
        ]);
        Company::create([
            'name' => 'Sungsang',
            'category_id' => '2',
            'desc' => 'elektronik',
            'tlpn' => '78338394',
            'email'=> 'sungsang@gmail.com',
            'slug' => 'sungsang',
            'address' => 'hauekskdbffm',
            'image' => ''
        ]);
        Company::create([
            'name' => 'Batique',
            'category_id' => '3',
            'desc' => 'Hotel yang bergerak dibidang',
            'tlpn' => '8218965',
            'email'=> 'batique@gmail.com',
            'slug' => 'batique',
            'address' => 'Jalan perintisan bajdk',
            'image' => ''
        ]);
        Company::create([
            'name' => 'Napoleonn',
            'category_id' => '1',
            'desc' => 'loremmmmenshvjvgvvjvdsd',
            'tlpn' => '7525373',
            'email'=> 'napoleon@gmail.com',
            'slug' => 'napoleon',
            'address' => 'Jalan perintisan bajdk',
            'image' => ''
        ]);
        Company::create([
            'name' => 'Sinte',
            'category_id' => '2',
            'desc' => 'banjbhwvdhjjdwd',
            'tlpn' => '7752635',
            'email'=> 'sinte@gmail.com',
            'slug' => 'sinte',
            'address' => 'hauekskdbffm',
            'image' => ''
        ]);
        Company::create([
            'name' => 'Redoorz',
            'category_id' => '3',
            'desc' => 'Hotel yang bergerak dibidang',
            'tlpn' => '3425677',
            'email'=> 'redoorz@gmail.com',
            'slug' => 'redoorz',
            'address' => 'Jalan perintisan bajdk',
            'image' => ''
        ]);
        Company::create([
            'name' => 'Sukimin',
            'category_id' => '1',
            'desc' => 'dhjhvjhavhjvd',
            'tlpn' => '7282998',
            'email'=> 'sukimin@gmail.com',
            'slug' => 'sukimin',
            'address' => 'Jalan perintisan bajdk',
            'image' => ''
        ]);
        Company::create([
            'name' => 'Panasonic',
            'category_id' => '2',
            'desc' => 'elektronik AC',
            'tlpn' => '3654321',
            'email'=> 'panasonic@gmail.com',
            'slug' => 'panasonic',
            'address' => 'hauekskdbffm',
            'image' => ''
        ]);
    }
}
